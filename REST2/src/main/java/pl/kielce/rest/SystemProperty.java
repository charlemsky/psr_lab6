package pl.kielce.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/SystemProperty")
public class SystemProperty {

	@GET
	@Path("/read")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProperty(@QueryParam("propertyName") String propertyName) {
		if (propertyName != null && propertyName.length() > 0)
			return propertyName + " : " + System.getProperty(propertyName, "PROPERTY NOT FOUND ");
		else
			return " NOT VALID PROPERTY NAME ";
	}
}
