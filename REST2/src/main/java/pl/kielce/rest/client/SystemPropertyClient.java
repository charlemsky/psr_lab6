package pl.kielce.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class SystemPropertyClient {
	public static void main(String[] args) {
		try {
			Client client = ClientBuilder.newClient();
			WebTarget target = client.target("http://localhost:8888/SystemProperty/read");
			Response response = target.queryParam("propertyName", "java.version").request().get();

			if (response.getStatus() != 200) {
				throw new RuntimeException("" + response.getStatus());
			}

			String output = response.readEntity(String.class);
			System.out.println(output);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}