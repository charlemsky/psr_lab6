package pl.kielce.rest;

import java.io.IOException;

import org.glassfish.grizzly.http.server.HttpServer;

import pl.kielce.rest.client.SystemPropertyClient;
import pl.kielce.rest.server.SystemPropertyServer;

public class Main {

	public static void main(String[] args) throws IOException {
		HttpServer httpServer = SystemPropertyServer.startupServer();
		SystemPropertyClient.main(null);
		SystemPropertyServer.shutdownServer(httpServer);	
	}

}
