package pl.kielce.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.kielce.rest.WebObject;

public class FactoryClient {
	public static void main(String[] args) {
		try {			
			Client client = ClientBuilder.newClient();
			WebTarget target = client.target("http://localhost:8888/Factory/create");

			Form form = new Form();
			form.param("id", "1");
			form.param("name", "super object");

			Entity<?> entity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED);
			
			Response response = target.request().accept(MediaType.APPLICATION_JSON).post(entity);
			
			if (response.getStatus() != 200) {
				throw new RuntimeException("" + response.getStatus());
			}
			
			WebObject output = response.readEntity(WebObject.class);
			System.out.println(output.getName());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}