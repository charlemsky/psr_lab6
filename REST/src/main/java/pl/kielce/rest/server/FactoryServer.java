package pl.kielce.rest.server;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

public class FactoryServer {

	public static HttpServer startupServer(){
		URI uri = URI.create("http://localhost:8888/");
		ResourceConfig rc = new ResourceConfig().packages("pl.kielce.rest");
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(uri, rc);
		System.out.println("Service started at " + uri);
		return httpServer;
	}
	
	public static void shutdownServer(HttpServer httpServer){
		httpServer.shutdownNow();
		System.out.println("Service shutdown");
	}
	
	public static void main(String[] args) throws IOException {
		HttpServer httpServer = startupServer();
		System.in.read();
		shutdownServer(httpServer);
	}
}