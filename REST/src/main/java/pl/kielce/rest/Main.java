package pl.kielce.rest;

import java.io.IOException;

import org.glassfish.grizzly.http.server.HttpServer;

import pl.kielce.rest.client.FactoryClient;
import pl.kielce.rest.server.FactoryServer;

public class Main {

	public static void main(String[] args) throws IOException {
		HttpServer httpServer = FactoryServer.startupServer();
		FactoryClient.main(null);
		FactoryServer.shutdownServer(httpServer);	
	}

}
