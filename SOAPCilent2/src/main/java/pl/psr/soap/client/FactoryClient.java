package pl.psr.soap.client;

import java.io.IOException;
import java.net.URL;

public class FactoryClient {

	public static void main(String[] args) throws IOException {			
		FactoryService service = new FactoryService();
		Factory port = service.getFactoryPort();
		WebObject webObject = port.createObject(1, "my object");
		System.out.println("WebObject = " + webObject.getId() + " : \"" + webObject.getName() + "\"");
		
		URL url = new URL(" http://localhost:9999/factoryService?wsdl");
		service = new FactoryService(url);
		port = service.getFactoryPort();
		webObject = port.createObject(2, "new object");
		System.out.println("WebObject = " + webObject.getId() + " : \"" + webObject.getName() + "\"");
	}

}
