package pl.psr.soap.client;

import java.io.IOException;
import java.net.URL;

public class CalculatorClient {

	public static void main(String[] args) throws IOException {	
		CalculatorService service = new CalculatorService();
		Calculator port = service.getCalculatorPort();
		int sum = port.add(1, 2);
		System.out.println("Sum: " + sum);
		
		URL url = new URL("http://localhost:9999/calculatorService?wsdl");
		service = new CalculatorService(url);
		port = service.getCalculatorPort();
		sum = port.add(1, 2);
		System.out.println("Sum: " + sum);
	}

}
