package pl.psr.soap.server;

import javax.xml.ws.Endpoint;

public class CalculatorServer {
	public static void main(String[] args) {		
		String address = "http://localhost:9999/calculatorService";
		Calculator calculatorService = new Calculator();
		Endpoint.publish(address, calculatorService);
		System.out.println("Service started at " + address);
	}
}
