package pl.psr.soap.server;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Calculator {

	@WebMethod
	public int add(int x, int y) {
		return x + y;
	}
}
