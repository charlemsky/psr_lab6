package pl.psr.soap.server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "WebObject" )
public class WebObject {

	@XmlElement
	private int id;

	@XmlElement
	private String name;
	
	public WebObject() {

	}

	public WebObject(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	
}
