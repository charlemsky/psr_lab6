package pl.psr.soap.server;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Factory {

	@WebMethod
	public WebObject createObject(int id, String name) {
		return new WebObject(id, name);
	}
}
